Simple technique to statically detect most of the wrong assignment of data types.

Very helpful in finding the line of code which can potentially create a security issue when code written for 32-bit ported to 64-bit compilation.

For instance, change in the size of "long" data type from 4 Byte to 8 Byte in 32 bit and 64 bit architecture respectively.
This change can potentially be vulnearable if the old code (written for 32 bit) is not modified or reviewed properly before compiling it for 64 bit architecture. 

